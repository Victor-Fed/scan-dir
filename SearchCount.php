<?php
namespace base;

use FilesystemIterator;

class SearchCount
{
    public string $path = __DIR__ . '/example-folder';
    public string $fileName = 'count';

    private int $sum = 0;
    /**
     * SearchCount constructor.
     * @param null $path
     * @param null $fileName
     */
    public function __construct($path = null, $fileName = null)
    {
        $this->path = $path ?: $this->path;
        $this->fileName = $fileName ?: $this->fileName;
    }

    /**
     * @return int|mixed
     */
    public function getSum()
    {
        $this->countByFiles($this->path);
        return $this->sum;
    }

    /**
     * @return array
     */
    protected function incrementSum($path)
    {
        foreach ($this->getNumbersFromFile($path) as $number) {
            $this->sum = $this->sum + $number;
        }
    }

    /**
     * @param $path
     * @return array|mixed
     */
    protected function getNumbersFromFile($path)
    {
        if ($text = trim(file_get_contents($path))) {
            preg_match_all('!\d+!', $text, $matches);
        }
        return $matches[0] ?? [];
    }

    /**
     * @param $path
     */
    protected function countByFiles($path)
    {
        $fi = new FilesystemIterator($path);
        foreach ($fi as $fileInfo) {
            if ($fileInfo->isFile()) {
                $realPath = $fileInfo->getRealPath();
                if (basename($realPath) === $this->fileName) {
                    $this->incrementSum($realPath);
                }
            } elseif ($fileInfo->isDir()) {
                $this->countByFiles($fileInfo->getRealPath());
            }
        }
    }
}
